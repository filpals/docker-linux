# Linux Docker

## Remarks
1. Please use a simple branch name in this project without domain i.e. `features/add-feature1`. Use `add-feature1` instead because it conflicts with the container registry naming system during pipeline build.
2. Please remove the existing container image if you want to push a new image. The pipeline build will not replace the old image if already exist.

## Requirements
- Docker

## Getting started
Build and run.

```bash
# Build
$ docker build -t ubuntu:latest .

# Run
$ docker run -it -v C:/:/mnt/c ubuntu:latest
```

## How to use
Add build job in your `.gitlab-ci.yml` as below.

```yml
stages:
  - build

build:
 stage: build
 image: registry.gitlab.com/filpals/docker-linux:latest
 script:
 - sshpass -p "PASSWORD" scp -i publickey.pub test.html administrator@172.0.0.1:/C:/inetpub/wwwroot/LicenseWeb/test.html
 artifacts:
  paths:
  - bin/Release/
```

## References
