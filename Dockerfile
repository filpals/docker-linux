FROM ubuntu:22.04

# Bring container up-to-date and install necessaries
RUN apt-get update && apt-get upgrade -y
RUN apt-get install sshpass -y

# Copy all files at current location into container
WORKDIR /
COPY . .

CMD ["bash"]